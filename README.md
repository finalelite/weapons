# Weapons

Plugin para a criação de armas e atribuí-las características especiais.

**Autor do código: Willian Gois / zMathi**

## Lista de armas
Cada categoria tem no mínimo seis armas diferentes, uma para cada raridade.

### Espadas
- Espada Gulosa (Comum) - Chance de restaurar a fome. *[+0.25% de chance a cada nível]*
- Espada Cega (Incomum) - Chance de dar cegueira ao inimigo por 3 segundos. *[+0.25 de chance a cada nível]*
- Espada das Nuvens (Raro) - Chance de jogar o inimigo para cima. *[+0.20 de chance a cada nível]*
- Espada Arranca-Coração (Épico) - Chance de roubar a vida do inimigo. *[+0.25% de chance a cada nível]*
- Espada de Gelo (Mítico) - Chance de congelar os inimigos por determinados segundos. *[+0.10 de chance a cada nível | +5 segundos de efeito a cada 25]*
- Espada de Lâmina Dupla (Lendário) - Chance de dobrar o dano causado. *[+0.35% de chance a cada nível]*

### Machados
- Machado Veloz (Comum) - Dá efeito de quebra-rápida por 10 segundos. *[+0.15% de chance de dar o efeito a cada nível | +1 nível de efeito a cada 25 níveis]*
- Machado Sangrento (Incomum) - Chance de deixar o inimigo sangrando por 8 segundos. *[+0.10% de chance a cada nível]*
- Machado Pesado (Raro) - Chance de dar uma pequena repulsão para frente no jogador, *[+0.40% de chance a cada nível]*
- Machado Furioso (Épico) - Chance de ganhar efeito de força por 15 segundos. *[+0.10% de chance a cada nível | +1 nível de efeito a cada 30 níveis]*
- Machado Viking (Mítico) - Chance de causar o dobro de dano em armaduras. *[+0.30 de chance a cada nível]*
- Machado Viking Lord (Lendário) - Chance de causar o dobro de dano em armaduras e ignorando totalmente encantos de proteção. *[+0.30% de chance a cada nível]*

### Capacetes
- Capacete de Água (Comum) - Chance de não ficar em fogo caso o inimigo tenha aspecto flamejante. *[+0.40% de chance por nível]*
- Capacete de Fogo (Incomum) - Chance de pôr fogo por um longo tempo no inimigo. *[+0.15% de chance por nível | +20 segundos de efeito a cada 20 níveis]*
- Capacete de Espinhos Venenosos (Raro) - Chance de envenenar o inimigo por determinados segundos. *[+0.25% de chance por nível | +2 segundos de efeito a cada 25 níveis]*
- Capacete Vulcânico (Raro) - Chance de atirar bolas de fogo para todos os lados. *[+0.15 de chance por nível]*
- Capacete de Fuga (Épico) - Chance de ser montado numa enderpear e fugir. *[+0.30% de chance a cada nível]*
- Capacete Repulsivo (Épico) - Chance de repulsar o inimigo. *[+0.10% de chance a cada nível]*
- Capacete Mágico (Mítico) - Chance de anular efeitos de outras armas. *[+0.25% de chance a cada nível]*
- Capacete do Capiroto (Mítico) - Chance de invocar três zombie pigmans na frente do jogador. *[+0.10 de chance a cada nível]*
- Capacete Mega-Proteção (Lendário) - Chance de dobrar o efeito do encanto de proteção. *[+0.25% de chance a cada nível]*

### Peitorais
- Peitoral de Água (Comum) - Chance de não ficar em fogo caso o inimigo tenha aspecto flamejante. *[+0.40% de chance por nível]*
- Peitoral de Fogo (Incomum) - Chance de pôr fogo por um longo tempo no inimigo. *[+0.15% de chance por nível | +20 segundos de efeito a cada 20 níveis]*
- Peitoral de Espinhos Venenosos (Raro) - Chance de envenenar o inimigo por determinados segundos. *[+0.25% de chance por nível | +2.5 segundos de efeito a cada 25 níveis]*
- Peitoral Vulcânico (Raro) - Chance de atirar bolas de fogo para todos os lados. *[+0.15 de chance por nível]*
- Peitoral de Fuga (Épico) - Chance de ser montado numa enderpear e fugir. *[+0.30% de chance a cada nível]*
- Peitoral Repulsivo (Épico) - Chance de repulsar o inimigo. *[+0.10% de chance a cada nível]*
- Peitoral Mágico (Mítico) - Chance de anular efeitos de outras armas. *[+0.25% de chance a cada nível]*
- Peitoral do Capiroto (Mítico) - Chance de invocar três zombie pigmans na frente do jogador. *[+0.10 de chance a cada nível]*
- Peitoral Mega-Proteção (Lendário) - Chance de dobrar o efeito do encanto de proteção. *[+0.25% de chance a cada nível]*

### Calças
- Calça de Água (Comum) - Chance de não ficar em fogo caso o inimigo tenha aspecto flamejante. *[+0.40% de chance por nível]*
- Calça de Fogo (Incomum) - Chance de pôr fogo por um longo tempo no inimigo. *[+0.15% de chance por nível | +20 segundos de efeito a cada 20 níveis]*
- Calça de Espinhos Venenosos (Raro) - Chance de envenenar o inimigo por determinados segundos. *[+0.25% de chance por nível | +2.5 segundos de efeito a cada 25 níveis]*
- Calça Vulcânica (Raro) - Chance de atirar bolas de fogo para todos os lados. *[+0.15 de chance por nível]*
- Calça de Fuga (Épico) - Chance de ser montado numa enderpear e fugir. *[+0.30% de chance a cada nível]*
- Calça Repulsivo (Épico) - Chance de repulsar o inimigo. *[+0.10% de chance a cada nível]*
- Calça Mágico (Mítico) - Chance de anular efeitos de outras armas. *[+0.25% de chance a cada nível]*
- Calça do Capiroto (Mítico) - Chance de invocar três zombie pigmans na frente do jogador. *[+0.10 de chance a cada nível]*
- Calça Mega-Proteção (Lendário) - Chance de dobrar o efeito do encanto de proteção. *[+0.25% de chance a cada nível]*

### Botas
- Botas de Água (Comum) - Chance de não ficar em fogo caso o inimigo tenha aspecto flamejante. *[+0.40% de chance por nível]*
- Botas de Fogo (Incomum) - Chance de pôr fogo por um longo tempo no inimigo. *[+0.15% de chance por nível | +20 segundos de efeito a cada 20 níveis]*
- Botas de Espinhos Venenosos (Raro) - Chance de envenenar o inimigo por determinados segundos. *[+0.25% de chance por nível | +2.5 segundos de efeito a cada 25 níveis]*
- Botas Vulcânica (Raro) - Chance de atirar bolas de fogo para todos os lados. *[+0.15 de chance por nível]*
- Botas de Fuga (Épico) - Chance de ser montado numa enderpear e fugir. *[+0.30% de chance a cada nível]*
- Botas Repulsivo (Épico) - Chance de repulsar o inimigo. *[+0.10% de chance a cada nível]*
- Botas Mágica (Mítico) - Chance de anular efeitos de outras armas. *[+0.25% de chance a cada nível]*
- Botas do Capiroto (Mítico) - Chance de invocar três zombie pigmans na frente do jogador. *[+0.10 de chance a cada nível]*
- Botas Mega-Proteção (Lendário) - Chance de dobrar o efeito do encanto de proteção. *[+0.25% de chance a cada nível]*

## Como usar
Usando a API do plugin é possivel criar e pegar armas.

### Criando e registrando uma arma

Abaixo, um exemplo de criação de uma espada rara que adiciona efeito de lentidão em uma entidade.
```java
//Parâmetros: Nome, descrição, item padrão, tipo, raridade e ação.
Weapons.getWeapons().getWeaponManager().addWeapon(new Weapon(
    "Espada Lenta",
    Arrays.asList( "§7Aplica lentidão em seus inimigos!"),
    new ItemStack(Material.DIAMOND_SWORD),
    WeaponType.SWORD,
    WeaponRarity.RARE,
    (player, other, weapon) -> {
        if (other instanceof LivingEntity)
            ((LivingEntity) other).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 5, 1));
    }
));
```

### Pegando o ItemStack

Com o código abaixo, é possível pegar um ItemStack que representará uma arma. O nome dever ser de uma já registrada.
```java
Weapons.getWeapons().getWeaponManager().getWeapon("Espada Lenta").getItem()
```